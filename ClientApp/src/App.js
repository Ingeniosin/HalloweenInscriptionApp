﻿import 'devextreme/dist/css/dx.material.teal.light.css';
import FormInscription from "./components/FormInscription";
import GridStudent from "./components/GridStudent";
import {Toaster} from "react-hot-toast";

const App = () => {
    return (
        <div className="container">
            <div className="row justify-content-center py-5">
                <div className="col-sm-12">
                    <GridStudent/>

                    <FormInscription/>

                </div>

            </div>

            <Toaster position="bottom-right" reverseOrder={false}/>

        </div>
    );
};

export default App;
