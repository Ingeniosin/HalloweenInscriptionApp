﻿import DataGrid, {Column, IDataGridOptions} from "devextreme-react/data-grid";
import {memo, useMemo} from "react";
import {getDsOptions} from "../../api/api-service";
import set from "lodash.set";

class DataGridConfig {
    columns: Array<Column>;
    defaultValues: any;
    columnChooser: boolean = true;
    allowAdding: boolean = true;
    allowDeleting: boolean = true;
    allowUpdating: boolean = true;
    dataSource: { api: string; filter: Array<any>; pageSize: number, select: Array<any> } | Array<any>;
    editorMode: 'row' | 'cell'
    reference: any;
    otherConfigs: IDataGridOptions;
    childs: any;
}

const requiredRule = {type: 'required', message: 'El campo es requerido'};
const emailRule = {type: 'email', message: 'El campo debe ser un email válido'};

const useCustomGrid = (config: DataGridConfig) => {
    const settings = useMemo(() => getGridProps(config), [config]);
    const columns = useMemo(() =>  settings.cols.map((col: Column) => <Column  {...col} key={col.dataField}/>), [settings.cols]);
    const childs = useMemo(() =>  settings.childs(), [settings.childs]);
    return (
        <DataGrid {...settings}>
            {columns}
            {childs}
        </DataGrid>
    );
}

const getGridProps = (config: DataGridConfig): IDataGridOptions => {
    let {defaultValues, childs, otherConfigs = {}, reference = {}, columnChooser = false, allowAdding = true, allowDeleting = true, allowUpdating = true, dataSource = [], columns, editorMode = 'cell'} = config;
    const isApi = typeof dataSource === 'object' && dataSource.api;
    if (isApi) {
        dataSource = getDsOptions(dataSource.api, {
            filter: dataSource.filter,
            select: [...columns.filter(c => c.dataField).map(c => c.dataField), ...(dataSource.select || [])],
            paginate: true,
            pageSize: dataSource.pageSize,
        })
    }

    columns = columns.map(column => {
        if(column.format) {
            set(column, 'editorOptions.format', column.format);
        }

        const validationRules = column.validationRules || [];

        if (column.required && column.dataType === 'boolean') {
            column.required = false;
        }

        if (column.required) {
            column.validationRules = [...validationRules, requiredRule];
        }



        if (column.email) {
            column.validationRules = [...validationRules, emailRule];
        }

        if (column.phone) {
            column.dataType = 'number';
            column.setCellValue = (rowData, value) => {
                rowData[column.dataField] = value.toString();
            }
        }

        return column;
    })
    return {
        showColumnHeaders: true,
        columnHidingEnabled: false,
        childs: childs || (() => null),
        cols: columns,
        loadPanel: {
            enabled: true,
            animation: true,
        },
        remoteOperations: {
            filtering: true,
            paging: true,
            sorting: true,
        },
        scrolling: {
            mode: "virtual",
            showScrollbar: "always",
            useNative: true
        },
       /* allowColumnResizing: true,*/
        highlightChanges: false,
        activeStateEnabled: false,
        cellHintEnabled: false,
        autoNavigateToFocusedRow: false,
        hoverStateEnabled: false,
        defaultSearchPanel: {
            visible: false,
            placeholder: 'Buscar'
        },
        dataSource: dataSource,
        defaultEditing: {
            mode: editorMode,
            allowAdding: allowAdding,
            allowDeleting: allowDeleting,
            allowUpdating: allowUpdating,
            useIcons: true,
            confirmDelete: false,
            editColumnName: true,
            texts: {
                addRow: "Añadir",
                cancelAllChanges: "Cancelar",
                cancelRowChanges: "Cancelar",
                confirmDeleteMessage: "¿Está seguro de que desea eliminar este registro?",
                deleteRow: "Eliminar",
                editRow: "Editar",
                saveAllChanges: "Guardar",
                saveRowChanges: "Guardar",
                undeleteRow: "Deshacer eliminación",
                validationCancelChanges: "Cancelar",
            }
        },
        /*repaintChangesOnly: true,*/
        showBorders: true,
        columnAutoWidth: true,
        wordWrapEnabled: false,
        allowColumnReordering: true,
        showColumnLines: true,
        columnResizingMode: 'widget',
        columnChooser: {
            enabled: columnChooser,
            allowSearch: true,
            mode: "select",
            title: "Selecciona las columnas",
            emptyPanelText: "No hay columnas que mostrar",
        },
        noDataText: '¡Nada por aqui!',
        onInitialized: ({component}) => {
            reference.current = {
                instance: component,
                refresh: async () => await component.refresh(),
                clearFilter: () => component.clearFilter(),
                clearSelection: () => component.clearSelection(),
                data: () => component.getDataSource().items(),
            };
        },
        onInitNewRow: e => {
            e.data = defaultValues;
        },
        ...otherConfigs
    }
}

export default useCustomGrid;
