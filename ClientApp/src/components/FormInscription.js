﻿import {useRef} from "react";
import {toast} from "react-hot-toast";
import {getDs} from "../api/api-service";
import CustomForm from "./devextreme/CustomForm";
import {SimpleItem, RequiredRule, ButtonItem}  from "devextreme-react/form";

const FormInscription = () => {
    const formRef = useRef(null)

    const onEnterKey = async  () => {
        const {isValidAsync, getData} = formRef.current;
        const isValid = await isValidAsync();
        if(!isValid) return;
        const data = getData();
        const response = await toast.promise(getDs("SignUp").insert(data), {
            loading: "Enviando solicitud...",
            success: "Solicitud enviada",
            error: "Error al enviar la solicitud"
        })
        console.log(response)
    };
    return (
        <>

         <CustomForm reference={formRef} formOptions={{
             labelMode: "floating",
         }} onEnterKey={onEnterKey}>

             <SimpleItem dataField="StudentId"  label={{
                 type: 'floating',
             }}>
                 <RequiredRule message="El ID de estudiante es necesario" />
             </SimpleItem>

             <ButtonItem horizontalAlignment="center" buttonOptions={{text: "Submit", type: "default", onClick: onEnterKey}} />

         </CustomForm>

        </>
    );
};

export default FormInscription;
