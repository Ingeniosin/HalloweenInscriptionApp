﻿import {useMemo, useRef} from "react";
import {getDsLookup} from "../api/api-service";
import useCustomGrid from "./../components/devextreme/useDefaultDataGrid";
import {
    Summary, TotalItem,
} from 'devextreme-react/data-grid';

const GridStudent = () => {
    const gridRef = useRef(null);
    const config = useMemo(() => ({
        reference: gridRef,
        dataSource: {
            api: "Inscriptions",
        },
        allowAdding: false,
        allowDeleting: false,
        columns: [
            {dataField: 'studentDataId', caption: 'Nombre estudiante', dataType: 'number', required: true, allowEditing: false, lookup: getDsLookup('Students', null, 'studentName')},
            //{dataField: 'studentData.studentId', caption: 'Student', dataType: 'string', required: true},
            {dataField: 'studentData.programName', caption: 'Programa', dataType: 'string', required: true, allowEditing: false,},
            {dataField: 'amount', caption: 'Dinero cancelado', dataType: 'number', required: true, format: 'currency'},
            {dataField: 'amountReturned', caption: 'Cambio', dataType: 'number', required: true, allowEditing: false, format: 'currency'},
            {dataField: 'isValid', caption: 'Pago finalizado', dataType: 'boolean'},
            {dataField: 'paymentDate', caption: 'Fecha de pago', dataType: 'datetime', allowEditing: false,},
            {dataField: 'inscriptionDate', caption: 'Fecha de inscripción', dataType: 'datetime', required: true, allowEditing: false,},
        ],
        childs: () => (
            <Summary>
                <TotalItem
                    column="amount"
                    summaryType="sum"

                    displayFormat="Importe total: {0}"
                    valueFormat="currency"
                />
            </Summary>
        )
    }), []);
    return useCustomGrid(config)
}

export default GridStudent;
