using DynamicApi;
using DynamicApi.Manager.Api;
using DynamicApi.Manager.Api.Managers;
using DynamicApi.Manager.Api.Managers.Action;
using DynamicApi.Manager.Api.Managers.Service;
using HalloweenInscriptionApp.Models;
using HalloweenInscriptionApp.Services;
using HalloweenInscriptionApp.Services.Action;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<FetchStudentDataService>();

new DynamicApi<ApplicationDbContext>(new List<IApiManager>{
    new ServiceApiManager<StudentInscription, StudentInscriptionService, ApplicationDbContext>("Inscriptions", x => x.Inscriptions),
    new NonServiceApiManager<StudentData, ApplicationDbContext>("Students", x => x.Students),
    new ActionApiManager<SignUpInput,SignUpAction>("SignUp"){IsScoped = true}
}, builder, null, _ => {
    new Thread(() => {
        while(true) {
            Console.WriteLine("Iniciando Login");
            FetchStudentDataService.Login().Wait();
            Console.WriteLine("Login finalizado, esperando 6 minutos");
            Thread.Sleep(1000 * 60 * 6);
        }
    }).Start();
    return Task.CompletedTask;
}).Start();
