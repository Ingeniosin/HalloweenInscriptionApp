﻿using HalloweenInscriptionApp.Models;
using Newtonsoft.Json;
using PuppeteerSharp;

namespace HalloweenInscriptionApp.Services; 

public class FetchStudentDataService {

    private static Page _page;
    private static Browser _browser;
    private static bool _isInitialized;
    private readonly static string _urlStudentData = "https://opscs.ucc.edu.co/psc/UCNAV_4/EMPLOYEE/SA/c/MANAGE_ACADEMIC_RECORDS.UC_SEMA_PLAN_EST.GBL?Page=UC_SEMA_PLAN_EST&Action=U&ACAD_CAREER=PREG&INSTITUTION=UCDEC";

    public static async Task Login() {
        _isInitialized = false;
        var page = await GetLoginPage();
        await page.GoToAsync("https://opscs.ucc.edu.co/psp/UCNAV/EMPLOYEE/SA/?&cmd=login&languageCd=ESP");
        await page.TypeAsync( "#userid", "juan.campino");
        await page.TypeAsync("#pwd", "Cdj7856*");
        await page.ClickAsync("#login > div > div.ps_signinentry > div:nth-child(8) > input");
        await page.WaitForTimeoutAsync(500);
        await page.GoToAsync(_urlStudentData);
        _isInitialized = true;
    }

    private static async Task<Page> GetLoginPage() {
        if (_page != null)
            return _page;
        _browser = await Puppeteer.LaunchAsync(new LaunchOptions{ Headless = true, });
        _page = await _browser.NewPageAsync();
        return _page;
    }

    private async Task<StudentData> GetStudentData(string studentId) {
        while (!_isInitialized) await Task.Delay(500);
        var func = $@"async () => {{
                    const loader = document.getElementById('WAIT_win4'), 
                          input = document.querySelector('#UC_PLAN_SEMA_VW_EMPLID'), 
                          button = document.querySelector('#win4divSEARCHBELOW > a:nth-child(3) > span > input');
                    input.value = '{studentId}';
                    button.click();                        
                    while(loader.style.visibility != 'hidden') {{
                        await new Promise(r => setTimeout(r, 10));
                    }}
                    var programId = document.querySelector('#UC_PLAN_SEMA_VW_ACAD_PLAN').innerText;
                    var programName = document.querySelector('#ACAD_PLAN_TBL_DESCR').innerText;
                    var studentName = document.querySelector('#UC_PEOPLE_SRCH_NAME_DISPLAY').innerText;
                    var studentId = document.querySelector('#UC_PLAN_SEMA_VW_EMPLID').innerText;
                    var creditsRequired = Number(document.querySelector('#UC_INSTITUT_WRK_UNITS_REQUIRED').innerText || '0');
                    var creditsDone = Number(document.querySelector('#UC_INSTITUT_WRK_SAA_UNITS_USED').innerText || '0');
                    var creditsLeft = Number(document.querySelector('#UC_INSTITUT_WRK_UNITS_NEEDED').innerText || '0');
                    var coursesRequired = Number(document.querySelector('#UC_INSTITUT_WRK_CRSES_REQUIRED').innerText || '0');
                    var coursesDone = Number(document.querySelector('#UC_INSTITUT_WRK_SAA_CRSES_USED').innerText || '0');
                    var coursesLeft = Number(document.querySelector('#UC_INSTITUT_WRK_CRSES_NEEDED').innerText || '0');
                    return JSON.stringify({{programId, programName, studentName, studentId, creditsRequired, creditsDone, creditsLeft, coursesRequired, coursesDone, coursesLeft}});
                }}";
        var result = await _page.EvaluateFunctionAsync<string>(func);
        var studentData = JsonConvert.DeserializeObject<StudentData>(result);
        
        new Thread(() => {
            _isInitialized = false;
            _page.ReloadAsync().Wait();
            _isInitialized = true;
        }).Start();
      
        return studentData;
    }

    public async Task<StudentData> GetStudentDataTry(string studentId) {
        try {
            return await GetStudentData(studentId);
        } catch (Exception e) {
            Console.WriteLine(e);
            await Login();
            return await GetStudentData(studentId);
        }
    }
}

