﻿using DynamicApi.Manager.Api.Managers.Action;
using HalloweenInscriptionApp.Models;
using Microsoft.EntityFrameworkCore;

namespace HalloweenInscriptionApp.Services.Action; 

public class SignUpAction : ActionService<SignUpInput> {
    
    private readonly FetchStudentDataService _fetchStudentDataService;
    private readonly ApplicationDbContext _db;

    public SignUpAction(FetchStudentDataService fetchStudentDataService, ApplicationDbContext db) {
        _fetchStudentDataService = fetchStudentDataService;
        _db = db;
    }

    public override async Task<object> OnQuery(SignUpInput model) {
        var studentId = model.StudentId.Trim().ToLower();
        var searchInscription = await _db.Inscriptions.Include(x => x.StudentData).FirstOrDefaultAsync(x => x.StudentData.StudentId.Trim().ToLower().Equals(studentId));
        if(searchInscription != null)
            throw new Exception("Ya te has inscrito");
        
        var inscription = new StudentInscription {
            StudentData = await _fetchStudentDataService.GetStudentDataTry(studentId),
        };
        await _db.Inscriptions.AddAsync(inscription);
        await _db.SaveChangesAsync();
        return inscription;
    }

    public override SignUpInput GetInstance() => new();
}

public class SignUpInput {
    
    public string StudentId { get; set; }
    
}