﻿using DynamicApi.Manager.Api.Managers.Service;
using HalloweenInscriptionApp.Models;

namespace HalloweenInscriptionApp.Services; 

public class StudentInscriptionService : ServiceModel<StudentInscription, ApplicationDbContext> {
    
    private readonly static double InscriptionPrice = 10000;

    public override Task<bool> OnUpdating(StudentInscription model, Func<bool, Task<StudentInscription>> getOldModel, Query query, ApplicationDbContext db) {
        if(!model.IsValid) {
            model.IsValid = model.Amount == InscriptionPrice;
        }
        
        model.AmountReturned = model.Amount - InscriptionPrice;

        model.PaymentDate = model.IsValid ?  (model.PaymentDate != null ? model.PaymentDate : DateTime.Now) : null;
        return base.OnUpdating(model, getOldModel, query, db);
    }

    public override ServiceConfiguration Configuration => new(){
        OnUpdating = true,
    };
}