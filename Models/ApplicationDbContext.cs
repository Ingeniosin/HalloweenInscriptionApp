﻿using DynamicApi.Manager;
using Microsoft.EntityFrameworkCore;

namespace HalloweenInscriptionApp.Models; 

public class ApplicationDbContext : DynamicDbContext {

    public ApplicationDbContext(DbContextOptions options) : base(options) {
    }
    
    public DbSet<StudentInscription> Inscriptions { get; set; }
    public DbSet<StudentData> Students { get; set; }


}