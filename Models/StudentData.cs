﻿using Microsoft.EntityFrameworkCore;

namespace HalloweenInscriptionApp.Models; 

[Index(nameof(StudentId), IsUnique = true)]
public class StudentData {
    public int Id { get; set; }

    public string StudentId { get; set; }
    public string StudentName { get; set; }
    public string ProgramId { get; set; }
    public string ProgramName { get; set; }
    public double CreditsRequired { get; set; }
    public double CreditsDone { get; set; }
    public double CreditsLeft { get; set; }
    public double CoursesRequired { get; set; }
    public double CoursesDone { get; set; }
    public double CoursesLeft { get; set; }
    
    public DateTime FetchDate { get; set; } = DateTime.Now;
    
}