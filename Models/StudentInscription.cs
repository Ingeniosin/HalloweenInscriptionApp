﻿namespace HalloweenInscriptionApp.Models; 

public class StudentInscription {
    public int Id { get; set; }
    
    public bool AlreadyPaid { get; set; }
    
    public double Amount { get; set; }
    public double AmountReturned { get; set; }
    public bool IsValid { get; set; }
    
    public DateTime? PaymentDate { get; set; }
    public DateTime InscriptionDate { get; set; } = DateTime.Now;
    
    public virtual StudentData StudentData { get; set; }
    public int StudentDataId { get; set; }
}